class Mult{
    public void print(int n){
        int factor = 1;
        if(!isPair(n)) factor = 2;
        for(int i=factor; i<=100; i+=factor){
            sms(n+ "\t*\t"+i+"\t=\t"+(n*i));
        }
    }
    private boolean isPair(int n){
        return n%2==0;
    }
    private void sms(String c){
        System.out.println(c);
    }
}