# How to use

- First: compile the `.java` files
```sh
$ javac *.java
```
- second: run the `Main` file
```sh
$ java Main
```
you must enter a number
```sh
$ javac *.java
Ingrese un numero
5
```
If everything goes well will show you by console the multiplication table of 1-100 of that number if the result is even, otherwise it will show an error message
```sh
Exception in thread "main" java.util.InputMismatchException
    .
    .
    .
```