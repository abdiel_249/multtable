import java.util.Scanner;
class Read{
    public static Integer getInt(){
        Scanner in = new Scanner(System.in);
        Integer n = null;
        try {
            n=in.nextInt();
            
        } catch(NumberFormatException e){
            System.err.println(e.getMessage());
        }
        in.close();
        return n;
    }
}